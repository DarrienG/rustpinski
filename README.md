# Sierpinski Triangle implementation (in rust!)

This is a simple implementation of a sierpinski triangle in rust.

![rustpinski](assets/rustpinski.png)

## Usage

Run the [binary](https://gitlab.com/DarrienG/rustpinski/-/releases) without args
and it will get rendering away immediately.

Some options are configurable:

```bash
$ rustpinski -h
rustpinski 1.0
Darrien Glasser <me@darrien.dev>
Render an OpenGL sierpinski triangle

USAGE:
    rustpinski [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -b, --batch <batch>        Set batch size to attempted to render at a time. Auto optimizes to a good value if left
                               unset.
    -m, --max-depth <depth>    Max triangle depth to render (May never finish at numbers past 9).
```

If you'd like faster render time, increase batch size with `-b`

If you'd like to render a greater depth of triangles, run with `-d` and set the
depth to something higher than the default of 6. Batch size automatically scales
with depth if left unset.

## Build requirements

Rust/cargo (I compiled with 1.47.0 but it will probably compile with lower versions)

Build with:

```bash
$ cargo build --release
```

## Behind the scenes

rustpinski uses OpenGL to render everything. It uses the [glium
library](https://crates.io/crates/glium) as an easy abstraction layer over
OpenGL.

Triangles are created in one thread and sent over a buffered channel to the
render thread.

With the default arguments, the triangle is rendered one triangle at a time
making for a nice smooth "animation". At higher batch sizes, render times are
decreased at the cost of smoothness letting more triangles be rendered in
between frames.

A simple abstraction layer over OpenGL concepts exists to users working with the
`Triangle` and `Vertex` object never have to think about things like GLSL and
work with a grid that is easier to reason with going from i16::MIN -> i16::MAX
rather than -1.0 -> 1.0.
