use colors::{PixelColor, COLORS};
use drawing::types::Triangle;
use std::{sync::mpsc::Sender, thread};
use types::Vertex;

pub mod types;
use crate::colors;
use crate::drawing;

pub fn start(drawable_sender: Sender<Triangle>, max_depth: u8) {
    thread::spawn(move || draw(drawable_sender, max_depth));
}

fn draw(drawable_sender: Sender<Triangle>, max_depth: u8) {
    drawable_sender
        .send(Triangle {
            vertices: [
                Vertex { x: 0, y: 30000 },
                Vertex {
                    x: 24515,
                    y: -30000,
                },
                Vertex {
                    x: -24515,
                    y: -30000,
                },
            ],
            color: COLORS[0],
        })
        .unwrap();

    let base = Triangle {
        vertices: [
            Vertex { x: 0, y: -30000 },
            Vertex { x: 12000, y: 100 },
            Vertex { x: -12000, y: 100 },
        ],
        color: COLORS[1],
    };

    drawable_sender.send(base).unwrap();
    draw_sierpinski(base, 1, max_depth, &drawable_sender);
    thread::park();
}

fn draw_sierpinski(base: Triangle, depth: u8, max_depth: u8, sender: &Sender<Triangle>) {
    if depth > max_depth {
        return;
    }

    let triangle_center = Vertex {
        x: base.vertices[0].x,
        y: base.vertices[1].y,
    };

    let move_distance = calculate_distance(base.vertices[1], base.vertices[2]);
    let triangle_color = COLORS[((depth * 3) as usize % COLORS.len())];

    let up_triangle = get_up_triangle(&triangle_center, move_distance, triangle_color);
    let left_triangle = get_left_triangle(&triangle_center, move_distance, triangle_color);
    let right_triangle = get_right_triangle(&triangle_center, move_distance, triangle_color);

    sender.send(up_triangle).unwrap();
    sender.send(left_triangle).unwrap();
    sender.send(right_triangle).unwrap();

    draw_sierpinski(up_triangle, depth + 1, max_depth, sender);
    draw_sierpinski(left_triangle, depth + 1, max_depth, sender);
    draw_sierpinski(right_triangle, depth + 1, max_depth, sender);
}

/// 🥊🥊🔻
fn get_up_triangle(triangle_center: &Vertex, move_distance: i16, color: PixelColor) -> Triangle {
    let new_triangle_len = (move_distance as f32 / 1.6) as i16;

    let left_center = triangle_center.x;
    Triangle {
        vertices: [
            Vertex {
                x: left_center,
                y: triangle_center.y,
            },
            Vertex {
                x: left_center - (new_triangle_len as f32 / 2.5) as i16,
                y: triangle_center.y + new_triangle_len,
            },
            Vertex {
                x: left_center + (new_triangle_len as f32 / 2.5) as i16,
                y: triangle_center.y + new_triangle_len,
            },
        ],
        color,
    }
}

fn get_left_triangle(triangle_center: &Vertex, move_distance: i16, color: PixelColor) -> Triangle {
    let new_triangle_len = (move_distance as f32 / 1.6) as i16;

    let left_center = triangle_center.x - (move_distance / 2 as i16);
    Triangle {
        vertices: [
            Vertex {
                x: left_center,
                y: triangle_center.y - (new_triangle_len * 2) as i16,
            },
            Vertex {
                x: left_center - (new_triangle_len as f32 / 2.5) as i16,
                y: triangle_center.y - new_triangle_len,
            },
            Vertex {
                x: left_center + (new_triangle_len as f32 / 2.5) as i16,
                y: triangle_center.y - new_triangle_len,
            },
        ],
        color,
    }
}

fn get_right_triangle(triangle_center: &Vertex, move_distance: i16, color: PixelColor) -> Triangle {
    let new_triangle_len = (move_distance as f32 / 1.6) as i16;

    let right_center = triangle_center.x + (move_distance / 2 as i16);
    Triangle {
        vertices: [
            Vertex {
                x: right_center,
                y: triangle_center.y - (new_triangle_len * 2) as i16,
            },
            Vertex {
                x: right_center - (new_triangle_len as f32 / 2.5) as i16,
                y: triangle_center.y - new_triangle_len,
            },
            Vertex {
                x: right_center + (new_triangle_len as f32 / 2.5) as i16,
                y: triangle_center.y - new_triangle_len,
            },
        ],
        color,
    }
}

/// Calculate the distance between 2 points.
/// Cannot be negative, but return type is set to i16 to ensure
/// it is bounded to valid values in the graph.
fn calculate_distance(vertex1: Vertex, vertex2: Vertex) -> i16 {
    // need to upcast for multiplication during distance algorithm
    // we can downcast immediately after
    let v1x = vertex1.x as i32;
    let v2x = vertex2.x as i32;
    let v1y = vertex1.y as i32;
    let v2y = vertex2.y as i32;
    f64::sqrt(((v1x - v2x).pow(2) + (v1y - v2y).pow(2)) as f64) as i16
}
