use rand::{rngs::StdRng, Rng};

pub const COLORS: [PixelColor; 118] = [
    PixelColor { r: 51, g: 0, b: 0 },
    PixelColor { r: 102, g: 0, b: 0 },
    PixelColor { r: 153, g: 0, b: 0 },
    PixelColor { r: 204, g: 0, b: 0 },
    PixelColor { r: 255, g: 0, b: 0 },
    PixelColor {
        r: 255,
        g: 51,
        b: 51,
    },
    PixelColor {
        r: 255,
        g: 51,
        b: 51,
    },
    PixelColor {
        r: 255,
        g: 102,
        b: 102,
    },
    PixelColor {
        r: 255,
        g: 153,
        b: 153,
    },
    PixelColor { r: 51, g: 25, b: 0 },
    PixelColor {
        r: 102,
        g: 51,
        b: 0,
    },
    PixelColor {
        r: 102,
        g: 51,
        b: 0,
    },
    PixelColor {
        r: 153,
        g: 76,
        b: 0,
    },
    PixelColor {
        r: 204,
        g: 102,
        b: 0,
    },
    PixelColor {
        r: 255,
        g: 128,
        b: 0,
    },
    PixelColor {
        r: 255,
        g: 153,
        b: 51,
    },
    PixelColor {
        r: 255,
        g: 178,
        b: 102,
    },
    PixelColor {
        r: 255,
        g: 204,
        b: 153,
    },
    PixelColor {
        r: 255,
        g: 229,
        b: 204,
    },
    PixelColor { r: 51, g: 51, b: 0 },
    PixelColor {
        r: 102,
        g: 102,
        b: 0,
    },
    PixelColor {
        r: 153,
        g: 153,
        b: 0,
    },
    PixelColor {
        r: 204,
        g: 204,
        b: 0,
    },
    PixelColor {
        r: 255,
        g: 255,
        b: 0,
    },
    PixelColor {
        r: 255,
        g: 255,
        b: 51,
    },
    PixelColor {
        r: 255,
        g: 255,
        b: 102,
    },
    PixelColor {
        r: 255,
        g: 255,
        b: 153,
    },
    PixelColor {
        r: 255,
        g: 255,
        b: 204,
    },
    PixelColor { r: 25, g: 51, b: 0 },
    PixelColor {
        r: 51,
        g: 102,
        b: 0,
    },
    PixelColor {
        r: 76,
        g: 153,
        b: 0,
    },
    PixelColor {
        r: 102,
        g: 204,
        b: 0,
    },
    PixelColor {
        r: 128,
        g: 255,
        b: 0,
    },
    PixelColor {
        r: 153,
        g: 255,
        b: 51,
    },
    PixelColor {
        r: 178,
        g: 255,
        b: 102,
    },
    PixelColor {
        r: 204,
        g: 255,
        b: 153,
    },
    PixelColor {
        r: 229,
        g: 255,
        b: 204,
    },
    PixelColor { r: 0, g: 51, b: 0 },
    PixelColor { r: 0, g: 102, b: 0 },
    PixelColor { r: 0, g: 153, b: 0 },
    PixelColor { r: 0, g: 204, b: 0 },
    PixelColor { r: 0, g: 255, b: 0 },
    PixelColor {
        r: 51,
        g: 255,
        b: 51,
    },
    PixelColor {
        r: 102,
        g: 255,
        b: 102,
    },
    PixelColor {
        r: 153,
        g: 255,
        b: 153,
    },
    PixelColor {
        r: 204,
        g: 255,
        b: 204,
    },
    PixelColor { r: 0, g: 51, b: 25 },
    PixelColor {
        r: 0,
        g: 102,
        b: 51,
    },
    PixelColor {
        r: 0,
        g: 153,
        b: 76,
    },
    PixelColor {
        r: 0,
        g: 204,
        b: 102,
    },
    PixelColor {
        r: 0,
        g: 255,
        b: 128,
    },
    PixelColor {
        r: 51,
        g: 255,
        b: 51,
    },
    PixelColor {
        r: 102,
        g: 255,
        b: 102,
    },
    PixelColor {
        r: 153,
        g: 255,
        b: 153,
    },
    PixelColor {
        r: 204,
        g: 255,
        b: 205,
    },
    PixelColor { r: 0, g: 51, b: 51 },
    PixelColor {
        r: 0,
        g: 102,
        b: 102,
    },
    PixelColor {
        r: 0,
        g: 153,
        b: 153,
    },
    PixelColor {
        r: 0,
        g: 204,
        b: 204,
    },
    PixelColor {
        r: 0,
        g: 255,
        b: 255,
    },
    PixelColor {
        r: 51,
        g: 255,
        b: 255,
    },
    PixelColor {
        r: 102,
        g: 255,
        b: 255,
    },
    PixelColor {
        r: 153,
        g: 255,
        b: 255,
    },
    PixelColor {
        r: 204,
        g: 255,
        b: 255,
    },
    PixelColor { r: 0, g: 25, b: 51 },
    PixelColor {
        r: 0,
        g: 51,
        b: 102,
    },
    PixelColor {
        r: 0,
        g: 76,
        b: 153,
    },
    PixelColor {
        r: 0,
        g: 102,
        b: 204,
    },
    PixelColor {
        r: 0,
        g: 128,
        b: 255,
    },
    PixelColor {
        r: 51,
        g: 153,
        b: 255,
    },
    PixelColor {
        r: 102,
        g: 178,
        b: 255,
    },
    PixelColor {
        r: 153,
        g: 204,
        b: 255,
    },
    PixelColor {
        r: 204,
        g: 229,
        b: 255,
    },
    PixelColor { r: 0, g: 0, b: 51 },
    PixelColor { r: 0, g: 0, b: 102 },
    PixelColor { r: 0, g: 0, b: 153 },
    PixelColor { r: 0, g: 0, b: 204 },
    PixelColor { r: 0, g: 0, b: 255 },
    PixelColor {
        r: 51,
        g: 51,
        b: 255,
    },
    PixelColor {
        r: 102,
        g: 102,
        b: 255,
    },
    PixelColor {
        r: 153,
        g: 153,
        b: 255,
    },
    PixelColor {
        r: 204,
        g: 204,
        b: 255,
    },
    PixelColor { r: 25, g: 0, b: 51 },
    PixelColor {
        r: 51,
        g: 0,
        b: 102,
    },
    PixelColor {
        r: 76,
        g: 0,
        b: 153,
    },
    PixelColor {
        r: 102,
        g: 0,
        b: 204,
    },
    PixelColor {
        r: 127,
        g: 0,
        b: 255,
    },
    PixelColor {
        r: 153,
        g: 51,
        b: 255,
    },
    PixelColor {
        r: 178,
        g: 102,
        b: 255,
    },
    PixelColor {
        r: 204,
        g: 153,
        b: 255,
    },
    PixelColor {
        r: 229,
        g: 204,
        b: 255,
    },
    PixelColor { r: 51, g: 0, b: 51 },
    PixelColor {
        r: 102,
        g: 0,
        b: 102,
    },
    PixelColor {
        r: 153,
        g: 0,
        b: 153,
    },
    PixelColor {
        r: 204,
        g: 0,
        b: 204,
    },
    PixelColor {
        r: 255,
        g: 0,
        b: 255,
    },
    PixelColor {
        r: 255,
        g: 51,
        b: 255,
    },
    PixelColor {
        r: 255,
        g: 102,
        b: 255,
    },
    PixelColor {
        r: 255,
        g: 153,
        b: 255,
    },
    PixelColor {
        r: 255,
        g: 204,
        b: 255,
    },
    PixelColor { r: 51, g: 0, b: 25 },
    PixelColor {
        r: 102,
        g: 0,
        b: 51,
    },
    PixelColor {
        r: 153,
        g: 0,
        b: 76,
    },
    PixelColor {
        r: 204,
        g: 0,
        b: 102,
    },
    PixelColor {
        r: 255,
        g: 0,
        b: 102,
    },
    PixelColor {
        r: 255,
        g: 51,
        b: 153,
    },
    PixelColor {
        r: 255,
        g: 102,
        b: 178,
    },
    PixelColor {
        r: 255,
        g: 153,
        b: 205,
    },
    PixelColor {
        r: 255,
        g: 204,
        b: 229,
    },
    PixelColor { r: 0, g: 0, b: 0 },
    PixelColor {
        r: 32,
        g: 32,
        b: 32,
    },
    PixelColor {
        r: 64,
        g: 64,
        b: 64,
    },
    PixelColor {
        r: 96,
        g: 96,
        b: 96,
    },
    PixelColor {
        r: 128,
        g: 128,
        b: 128,
    },
    PixelColor {
        r: 160,
        g: 160,
        b: 160,
    },
    PixelColor {
        r: 192,
        g: 192,
        b: 192,
    },
    PixelColor {
        r: 224,
        g: 224,
        b: 225,
    },
    PixelColor {
        r: 255,
        g: 255,
        b: 255,
    },
];

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash)]
pub struct PixelColor {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

impl PixelColor {
    #[must_use]
    pub fn pick_rand(rng: &mut StdRng) -> PixelColor {
        COLORS[rng.gen_range(0, COLORS.len())]
    }
}
