use colors::PixelColor;

use crate::colors;

#[derive(Debug, Copy, Clone)]
pub struct Triangle {
    pub vertices: [Vertex; 3],
    pub color: PixelColor,
}

impl Triangle {
    pub fn get_vbuf(&self, display: &glium::Display) -> glium::VertexBuffer<OpenGlVertex> {
        glium::VertexBuffer::new(
            display,
            &self
                .vertices
                .iter()
                .map(|vertex| OpenGlVertex::from(*vertex))
                .collect::<Vec<OpenGlVertex>>(),
        )
        .unwrap()
    }
    pub fn get_fragment_shader_glsl(&self) -> String {
        format!(
            r#"
        #version 140
        out vec4 color;
        void main() {{
            color = vec4({}, {}, {}, 1.0);
        }}
            "#,
            (self.color.r as f32) / (u8::MAX as f32),
            (self.color.g as f32) / (u8::MAX as f32),
            (self.color.b as f32) / (u8::MAX as f32)
        )
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Vertex {
    pub x: i16,
    pub y: i16,
}

#[derive(Copy, Clone, Debug)]
pub struct OpenGlVertex {
    position: [f32; 2],
}

impl From<Vertex> for OpenGlVertex {
    fn from(vertex: Vertex) -> Self {
        OpenGlVertex {
            position: [
                (vertex.x as f32) / (i16::MAX as f32),
                (vertex.y as f32) / (i16::MAX as f32),
            ],
        }
    }
}

glium::implement_vertex!(OpenGlVertex, position);
