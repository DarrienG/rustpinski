use clap::{App, Arg};
use drawing::types::Triangle;
use std::{
    process::exit,
    sync::{
        mpsc,
        mpsc::{Receiver, Sender},
    },
};

pub mod colors;
pub mod drawing;
mod grid;

fn main() {
    let matches = App::new("rustpinski")
        .version("1.0")
        .author("Darrien Glasser <me@darrien.dev>")
        .about("Render an OpenGL sierpinski triangle")
        .arg(
            Arg::with_name("batch")
                .short("b")
                .long("batch")
                .help("Set batch size to attempted to render at a time. Auto optimizes to a good value if left unset.")
                .takes_value(true)
                .required(false),
        )
        .arg(
            Arg::with_name("depth")
                .short("m")
                .long("max-depth")
                .help("Max triangle depth to render (May never finish at numbers past 9).")
                .takes_value(true)
                .required(false),
        )
        .get_matches();

    let max_depth = matches
        .value_of("depth")
        .unwrap_or("6")
        .parse::<u8>()
        .unwrap_or_else(|_| {
            eprintln!("Depth must be a valid number from: 0-{}", u8::MAX);
            exit(1);
        });

    let batch_size = matches
        .value_of("batch")
        .unwrap_or_else(|| calculate_batch(max_depth))
        .parse::<u16>()
        .unwrap_or_else(|_| {
            eprintln!("Depth must be a valid number from: 0-{}", u16::MAX);
            exit(1);
        });

    let (drawable_sender, drawable_receiever): (Sender<Triangle>, Receiver<Triangle>) =
        mpsc::channel();

    drawing::start(drawable_sender, max_depth);
    grid::start(drawable_receiever, batch_size);
}

fn calculate_batch<'a>(max_depth: u8) -> &'a str {
    match max_depth {
        0..=6 => "1",
        7 => "2",
        8 => "10",
        9 => "100",
        10 => "200",
        11 => "400",
        _ => "1000",
    }
}
