use colors::PixelColor;
use drawing::types::Triangle;
use glium::{glutin, Program, Surface};
use std::{collections::HashMap, sync::mpsc::Receiver};

use crate::colors;
use crate::drawing;

pub fn start(drawable_receiver: Receiver<Triangle>, batch_size: u16) {
    let event_loop = glutin::event_loop::EventLoop::new();
    let wb = glutin::window::WindowBuilder::new().with_title("rustpinski");
    let cb = glutin::ContextBuilder::new();
    let display = glium::Display::new(wb, cb, &event_loop).unwrap();

    let vertex_shader_src = r#"
        #version 140
        in vec2 position;
        void main() {
            gl_Position = vec4(position, 0.0, 1.0);
        }
    "#;

    let mut programs: HashMap<PixelColor, Program> = HashMap::new();

    let mut to_draw = vec![];

    event_loop.run(move |event, _, control_flow| {
        let next_frame_time =
            std::time::Instant::now() + std::time::Duration::from_nanos(16_666_667);
        *control_flow = glutin::event_loop::ControlFlow::WaitUntil(next_frame_time);

        match event {
            glutin::event::Event::WindowEvent { event, .. } => match event {
                glutin::event::WindowEvent::CloseRequested => {
                    *control_flow = glutin::event_loop::ControlFlow::Exit;
                    return;
                }
                _ => return,
            },
            glutin::event::Event::NewEvents(cause) => match cause {
                glutin::event::StartCause::ResumeTimeReached { .. } => (),
                glutin::event::StartCause::Init => (),
                _ => return,
            },
            _ => return,
        }

        let mut target = display.draw();
        target.clear_color(0.20, 0.71, 1.0, 1.0);

        for _ in 0..batch_size + 1 {
            if let Ok(v) = drawable_receiver.try_recv() {
                to_draw.push(v);
                if programs.get(&v.color).is_none() {
                    programs.insert(
                        v.color,
                        Program::from_source(
                            &display,
                            vertex_shader_src,
                            &v.get_fragment_shader_glsl(),
                            None,
                        )
                        .unwrap(),
                    );
                }
            }
        }
        to_draw.iter().for_each(|item| {
            target
                .draw(
                    &item.get_vbuf(&display),
                    &glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList),
                    &programs.get(&item.color).expect(
                        "Missing item from program map. All received triangles must immediately store color in color map when received."
                    ),
                    &glium::uniforms::EmptyUniforms,
                    &Default::default(),
                )
                .unwrap()
        });
        target.finish().unwrap();
    });
}
