all:
	cargo build --release
	strip target/release/rustpinski

check:
	cargo test
	cargo clippy -- -D warnings
